#!/usr/bin/make -f

all: rep-seqs.qzv

sample-metadata.tsv:
	wget -O "sample-metadata.tsv" "https://data.qiime2.org/2021.4/tutorials/fmt/sample_metadata.tsv"


fmt-tutorial-demux-1.qza:
	wget -O "fmt-tutorial-demux-1.qza" "https://data.qiime2.org/2021.4/tutorials/fmt/fmt-tutorial-demux-1-1p.qza"

fmt-tutorial-demux-2.qza:
	wget -O "fmt-tutorial-demux-2.qza" "https://data.qiime2.org/2021.4/tutorials/fmt/fmt-tutorial-demux-2-1p.qza"


demux-summary-1.qzv demux-summary-2.qzv: fmt-tutorial-demux-1.qza fmt-tutorial-demux-2.qza
	qiime demux summarize --i-data fmt-tutorial-demux-1.qza --o-visualization demux-summary-1.qzv
	qiime demux summarize --i-data fmt-tutorial-demux-2.qza --o-visualization demux-summary-2.qzv


rep-seqs-1.qza table-1.qza stats-1.qza rep-seqs-2.qza table-2.qza stats-2.qza: fmt-tutorial-demux-1.qza  fmt-tutorial-demux-2.qza
	qiime dada2 denoise-single \
	  --p-trim-left 13 --p-trunc-len 150 \
	  --i-demultiplexed-seqs fmt-tutorial-demux-1.qza \
	  --o-representative-sequences rep-seqs-1.qza \
	  --o-table table-1.qza \
	  --o-denoising-stats stats-1.qza
	qiime dada2 denoise-single \
	  --p-trim-left 13 --p-trunc-len 150 \
	  --i-demultiplexed-seqs fmt-tutorial-demux-2.qza \
	  --o-representative-sequences rep-seqs-2.qza \
	  --o-table table-2.qza \
	  --o-denoising-stats stats-2.qza

denoising-stats-1.qzv denoising-stats-2.qzv: stats-1.qza  stats-2.qza
	qiime metadata tabulate --m-input-file stats-1.qza --o-visualization denoising-stats-1.qzv
	qiime metadata tabulate --m-input-file stats-2.qza --o-visualization denoising-stats-2.qzv

table.qza rep-seqs.qza: table-1.qza table-2.qza rep-seqs-1.qza rep-seqs-2.qza
	qiime feature-table merge \
	  --i-tables table-1.qza \
	  --i-tables table-2.qza \
	  --o-merged-table table.qza
	qiime feature-table merge-seqs \
	  --i-data rep-seqs-1.qza \
	  --i-data rep-seqs-2.qza \
	  --o-merged-data rep-seqs.qza

table.qzv: sample-metadata.tsv table.qza
	qiime feature-table summarize \
	  --i-table table.qza \
	  --o-visualization table.qzv \
	  --m-sample-metadata-file sample-metadata.tsv

rep-seqs.qzv: rep-seqs.qza
	qiime feature-table tabulate-seqs \
	  --i-data rep-seqs.qza \
	  --o-visualization rep-seqs.qzv


