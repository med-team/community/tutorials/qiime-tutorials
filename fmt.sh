#!/bin/bash

set -e

if [ ! -x /usr/bin/realpath ]; then
	echo "E: Missing realpath, please install the coreutils package"
	exit 1
fi

p=$(realpath $0)
m=$(echo $p | sed -e 's/\.sh$/.make/')
t=$(basename $p .sh)

echo $t

mkdir -p ${t}.tmpdir
cd ${t}.tmpdir
make -f $m all
